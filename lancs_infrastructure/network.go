package infrastructure

import (
	"encoding/json"
	"io"
	"net/http"
	"time"
)

type RuleList struct {
	Version int    `json:"version"`
	Rules   []Rule `json:"rules"`
}

type HostList struct {
	Version int    `json:"version"`
	Hosts   []Host `json:"hosts"`
}

func HandleGetResponse[T any](url string) (T, error) {
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	response, err := client.Get(url)
	if err != nil {
		return *new(T), err
	}
	defer response.Body.Close()

	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		return *new(T), err
	}

	var result T
	err = json.Unmarshal(responseData, &result)
	if err != nil {
		return *new(T), err
	}

	return result, nil
}

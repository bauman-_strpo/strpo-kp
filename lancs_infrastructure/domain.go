package infrastructure

type Rule struct {
	DestIP string `json:"destip"`
	SrcIP  string `json:"srcip"`
	Domain string `json:"domain"`
}

type Host struct {
	Tag  string `json:"tag"`
	IP   string `json:"ip"`
	Port string `json:"port"`
}

func FindByTag(hosts []Host, tag string) (int, *Host) {
	for i, host := range hosts {
		if host.Tag == tag {
			return i, &host
		}
	}
	return 0, nil
}

func FindByIP(hosts []Host, ip string) (int, *Host) {
	for i, host := range hosts {
		if host.IP == ip {
			return i, &host
		}
	}
	return 0, nil
}

func FindByAddress(rules []Rule, address string) (int, Rule) {
	for i, rule := range rules {
		if rule.DestIP == address || rule.SrcIP == address || rule.Domain == address {
			return i, rule
		}
	}
	return 0, *new(Rule)
}

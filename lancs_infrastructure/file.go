package infrastructure

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/mitchellh/mapstructure"
	"github.com/zhevron/go-config"
)

func GetConfig() string {
	if os.Getenv("APPDATA") != "" {
		return filepath.Join(os.Getenv("APPDATA"), "lancs", "lancsd.conf")
	}

	if os.Getenv("XDG_CONFIG_HOME") != "" {
		return filepath.Join(os.Getenv("XDG_CONFIG_HOME"), "lancs", "lancsd.conf")
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(homeDir, ".config", "lancs", "lancsd.conf")
}

func GetHosts() (hosts []Host) {

	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	hostsData := config.Get("hosts", *new([]any)).([]interface{})
	unmarshallArrayFromConfig[Host](hostsData, &hosts)

	return hosts
}

func GetRules() (rules []Rule) {

	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	rulesData := config.Get("rules", *new([]any)).([]interface{})
	unmarshallArrayFromConfig[Rule](rulesData, &rules)

	return rules
}

func GetPort() int {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	return int(config.GetFloat64("port", 32458))
}

func SetPort(port int) error {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		fmt.Print(err.Error())
	}

	config.Set("port", port)

	err := config.SaveFileJSON(GetConfig())

	return err
}

func SetRules(newRules []Rule) error {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	config.Set("rules", newRules)
	err := config.SaveFileJSON(GetConfig())

	return err
}

func SetHosts(newHosts []Host) error {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	config.Set("hosts", newHosts)
	err := config.SaveFileJSON(GetConfig())

	return err
}

func ClearRules() error {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	config.Remove("rules")

	return config.SaveFileJSON(GetConfig())
}

func ClearHosts() error {
	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}

	config.Remove("hosts")

	return config.SaveFileJSON(GetConfig())
}

func unmarshallArrayFromConfig[T any](input []interface{}, output *[]T) {
	for _, data := range input {
		var obj T
		mapstructure.Decode(data, &obj)
		*output = append(*output, obj)
	}
}

package infrastructure

import (
	"fmt"
)

func Contains[T comparable](s []T, e T) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}

func RemoveByIndex[T interface{}](slice []T, s int) ([]T, error) {
	if s >= len(slice) {
		return *new([]T), fmt.Errorf("index out of range")
	}
	return append(slice[:s-1], slice[s+1:]...), nil
}

func RemoveByElement[T comparable](slice []T, s T) ([]T, error) {
	index, err := _find[T](slice, s)
	return append(slice[:index-1], slice[index+1:]...), err
}

func _find[T comparable](s []T, e T) (int, error) {

	for i, v := range s {
		if v == e {
			return i, nil
		}
	}
	return 0, fmt.Errorf("element not found")
}

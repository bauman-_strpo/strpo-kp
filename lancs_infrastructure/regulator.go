package infrastructure

import (
	"net/rpc"

	"github.com/hashicorp/go-plugin"
)

type Regulator interface {
	Apply(rules []Rule) error
}

type RegulatorRPC struct{ client *rpc.Client }

func (r *RegulatorRPC) Apply(rules []Rule) error {
	var resp error
	err := r.client.Call("Plugin.Apply", rules, &resp)
	if err != nil {
		return err
	}
	return resp
}

type RegulatorRPCServer struct {
	Impl Regulator
}

func (s *RegulatorRPCServer) Apply(args interface{}, resp *error) error {
	s.Impl.Apply((args).([]Rule))
	return nil
}

type RegulatorPlugin struct {
	Impl Regulator
}

func (p RegulatorPlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &RegulatorRPCServer{Impl: p.Impl}, nil
}

func (RegulatorPlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &RegulatorRPC{client: c}, nil
}

var HandshakeConfig = plugin.HandshakeConfig{
	ProtocolVersion:  1,
	MagicCookieKey:   "BASIC_PLUGIN",
	MagicCookieValue: "hello",
}

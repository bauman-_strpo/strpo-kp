package main

import (
	"fmt"
	"infrastructure"
	"net"

	"github.com/coreos/go-iptables/iptables"
	"github.com/hashicorp/go-plugin"
)

func main() {
	regulator := &IptablesRegulator{}

	var pluginMap = map[string]plugin.Plugin{
		"regulator": &infrastructure.RegulatorPlugin{Impl: regulator},
	}

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: infrastructure.HandshakeConfig,
		Plugins:         pluginMap,
	})
}

/*
func main() {
	regulator := &IptablesRegulator{}

	regulator.Apply([]infrastructure.Rule{{DestIP: "1.1.1.1"}})
}
*/

type IptablesRegulator struct {
}

func (reg *IptablesRegulator) Apply(rules []infrastructure.Rule) error {

	var err error
	err = nil

	ipt, err := iptables.New()

	for _, rule := range rules {
		if rule.DestIP != "" {
			err := ipt.AppendUnique("filter", "OUTPUT", "-p tcp -d "+rule.DestIP)
			if err != nil {
				return err
			}
		} else if rule.SrcIP != "" {
			err := ipt.AppendUnique("filter", "INPUT", "-p tcp -s "+rule.SrcIP)
			if err != nil {
				return err
			}
		} else if rule.Domain != "" {
			err := ipt.AppendUnique("filter", "OUTPUT", "-p tcp -d "+rule.Domain)
			if err != nil {
				return err
			}
			err = ipt.AppendUnique("filter", "INPUT", "-p tcp -s "+rule.Domain)
			if err != nil {
				return err
			}
		} else {
			return fmt.Errorf("wrong rule format")
		}
	}
	return err
}

func (reg IptablesRegulator) resolveDomain(domain string) string {
	var destIP *net.IPAddr
	var err error
	if destIP, err = net.ResolveIPAddr("ip4", domain); err != nil {
		return ""
	}
	return destIP.IP.String()
}

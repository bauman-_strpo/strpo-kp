package main

import (
	"encoding/json"
	"infrastructure"
	"log"
	"net/http"
	"reflect"
	"strings"

	"github.com/gin-gonic/gin"
)

func getRules(c *gin.Context) {
	resp := infrastructure.RuleList{Version: RulesVer, Rules: activeRules}
	c.IndentedJSON(http.StatusOK, resp)
}

func getHosts(c *gin.Context) {
	resp := infrastructure.HostList{Version: HostsVer, Hosts: activeHosts}
	c.IndentedJSON(http.StatusOK, resp)
}

func modifyRules(c *gin.Context) {
	//var _data interface{}
	var _list infrastructure.RuleList

	if err := c.BindJSON(&_list); err != nil {
		log.Print(err.Error())
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
		return
	}

	// _list, ok := _data.(infrastructure.RuleList)
	// if !ok {
	// 	log.Print("couldn't convert rulelist")
	// 	c.IndentedJSON(http.StatusInternalServerError, "couldn't convert rulelist")
	// 	return
	// }

	if _list.Version < RulesVer {
		log.Print("old version recieved")
		c.IndentedJSON(http.StatusNotAcceptable,
			infrastructure.RuleList{Version: RulesVer, Rules: activeRules})
		return
	}

	if _list.Version == RulesVer && reflect.DeepEqual(_list.Rules, activeRules) {
		c.IndentedJSON(http.StatusAlreadyReported,
			infrastructure.RuleList{Version: RulesVer, Rules: nil})
		return
	}

	if _list.Version == RulesVer && !reflect.DeepEqual(_list.Rules, activeRules) {
		log.Print("sync error: version match, rules mismatch")
	}

	activeRules = _list.Rules
	RulesVer = _list.Version
	c.IndentedJSON(http.StatusAccepted,
		infrastructure.RuleList{Version: RulesVer, Rules: activeRules})

	infrastructure.SetRules(activeRules)

	go updateRules()
}

func modifyHosts(c *gin.Context) {
	var _data interface{}
	var _list infrastructure.HostList

	if err := c.BindJSON(&_data); err != nil {
		log.Print(err.Error())
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
		return
	}

	_list, ok := _data.(infrastructure.HostList)
	if !ok {
		log.Print("couldn't convert hostlist")
		c.IndentedJSON(http.StatusInternalServerError, "couldn't convert hostlist")
		return
	}

	if _list.Version < HostsVer {
		log.Print("old version recieved")
		c.IndentedJSON(http.StatusNotAcceptable,
			infrastructure.HostList{Version: HostsVer, Hosts: activeHosts})
		return
	}

	if _list.Version == HostsVer && reflect.DeepEqual(_list.Hosts, activeHosts) {
		c.IndentedJSON(http.StatusAlreadyReported,
			infrastructure.HostList{Version: HostsVer, Hosts: nil})
		return
	}

	if _list.Version == HostsVer && !reflect.DeepEqual(_list.Hosts, activeHosts) {
		log.Print("sync error: version match, hosts mismatch")
	}

	activeHosts = _list.Hosts
	HostsVer = _list.Version
	c.IndentedJSON(http.StatusAccepted,
		infrastructure.HostList{Version: HostsVer, Hosts: activeHosts})

	infrastructure.SetHosts(activeHosts)

	go updateHosts()
}

func updateRules() {

	_list := infrastructure.RuleList{Version: HostsVer, Rules: activeRules}

	_json, err := json.Marshal(_list)
	if err != nil {
		log.Print(err.Error())
		return
	}

	reader := strings.NewReader(string(_json))

	for _, _host := range activeHosts {

		reader.Reset(string(_json))

		req, err := http.NewRequest(http.MethodPut, "http://"+_host.IP+_host.Port+"/rules", reader)
		if err != nil {
			return
		}

		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		response, err := client.Do(req)
		if err != nil {
			return
		}
		defer response.Body.Close()

		//work on response
	}
}

func updateHosts() {
	_list := infrastructure.HostList{Version: HostsVer, Hosts: activeHosts}

	_json, err := json.Marshal(_list)
	if err != nil {
		log.Print(err.Error())
		return
	}

	reader := strings.NewReader(string(_json))

	for _, _host := range activeHosts {

		reader.Reset(string(_json))

		req, err := http.NewRequest(http.MethodPut, "http://"+_host.IP+_host.Port+"/rules", reader)
		if err != nil {
			return
		}

		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		response, err := client.Do(req)
		if err != nil {
			return
		}
		defer response.Body.Close()

		//if response.Header.Get("")
		//work on response
	}
}

/*
func addRule(c *gin.Context) {
	var _data interface{}
	var _rules []infrastructure.Rule

	c.Request.Body
	if err := c.BindJSON(&_data); err != nil {
		log.Print(err.Error())
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
		return
	}
	if infrastructure.IsSlice(_data) {
		_slice := (_data).([]map[string]interface{})
		mapstructure.Decode(_slice, &_rules)
	} else {
		_obj := (_data).(map[string]interface{})
		var _rule infrastructure.Rule
		//err := mapstructure.Decode(_obj, &_rule)
		err := json.Unmarshal()
		if err != nil {
			log.Print(err.Error())
		}
		_rules = append(_rules, _rule)
	}

	if err := infrastructure.AddRule(_rules...); err != nil {
		log.Print(err.Error())
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
		return
	}

	activeRules = append(activeRules, _rules...)

	c.IndentedJSON(http.StatusCreated, activeRules)
}

func addHost(c *gin.Context) {
	var _hosts []infrastructure.Host

	if err := c.BindJSON(&_hosts); err != nil {
		log.Print(err.Error())
		c.IndentedJSON(http.StatusInternalServerError, err.Error())
		return
	}

	activeHosts = append(activeHosts, _hosts...)
	infrastructure.AddHosts(_hosts...)

	c.IndentedJSON(http.StatusCreated, activeHosts)
}
*/
/*func deleteRule(c *gin.Context) {
	address := c.Param("address")

	_, rule := infrastructure.FindByAddress(activeRules, address)
	infrastructure.RemoveRule(rule)
	for i, _rule := range activeRules {
		if infrastructure.Contains(activeRules, _rule) {
			activeRules = append(activeRules[:i-1], activeRules[:i+1]...)
		}
	}

	c.IndentedJSON(http.StatusAccepted, activeRules)
}

func deleteHost(c *gin.Context) {
	tag := c.Param("tag")

	_, host := infrastructure.FindByTag(activeHosts, tag)
	infrastructure.RemoveHost(*host)
	for i, _host := range activeHosts {
		if infrastructure.Contains(activeHosts, _host) {
			activeHosts = append(activeHosts[:i-1], activeHosts[:i-1]...)
		}
	}

	c.IndentedJSON(http.StatusAccepted, activeRules)
}
*/

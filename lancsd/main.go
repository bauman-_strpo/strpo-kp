package main

import (
	"infrastructure"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/hashicorp/go-plugin"
)

var (
	activeRules []infrastructure.Rule
	RulesVer    int
	activeHosts []infrastructure.Host
	HostsVer    int
	activePort  int
)

func main() {
	activePort = infrastructure.GetPort()

	activeHosts = infrastructure.GetHosts()

	if activeHosts != nil {
		for _, host := range activeHosts {
			var err error
			activeRules, err = infrastructure.HandleGetResponse[[]infrastructure.Rule]("http://" + host.IP + ":" + host.Port + "/rules")
			if err != nil {
				log.Printf("no connection to %s due to error:\n%s", host.IP, err.Error())
			}
		}
	}
	if activeRules == nil {
		activeRules = infrastructure.GetRules()
	} else {
		infrastructure.SetRules(activeRules)
	}

	go setupRegulator()

	router := gin.Default()

	router.GET("/rules", getRules)
	router.PUT("/rules", modifyRules)
	router.GET("/hosts", getHosts)
	router.PUT("/hosts", modifyHosts)

	srv := &http.Server{
		Addr:    ":" + strconv.Itoa(activePort),
		Handler: router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")
}

func setupRegulator() {
	client := plugin.NewClient(&plugin.ClientConfig{
		HandshakeConfig: infrastructure.HandshakeConfig,
		Plugins:         pluginMap,
		Cmd:             exec.Command("regulator_iptables"),
	})
	defer client.Kill()

	rpcClient, err := client.Client()
	if err != nil {
		log.Fatal(err)
	}

	raw, err := rpcClient.Dispense("regulator")
	if err != nil {
		log.Fatal(err)
	}

	regulator := raw.(infrastructure.Regulator)

	ticker := time.NewTicker(30 * time.Second)

	for range ticker.C {
		regulator.Apply(activeRules)
	}
}

var pluginMap = map[string]plugin.Plugin{
	"regulator": &infrastructure.RegulatorPlugin{},
}

package main

import (
	"fmt"
	"os"
)

var (
	a Application
	t TerminalOutput
)

type TerminalOutput struct {
}

func (t TerminalOutput) Out(s any) {
	fmt.Print(s)
}

func main() {

	args := os.Args

	if err := a.run(t, args); err != nil {
		t.Out(fmt.Errorf(err.Error()))
		return
	}
}

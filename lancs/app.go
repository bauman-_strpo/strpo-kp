package main

import (
	"encoding/json"
	"fmt"
	"infrastructure"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/biter777/processex"
)

type Application struct {
	_out infrastructure.Output
}

func (a Application) run(out infrastructure.Output, args []string) error {
	var err error
	a._out = out

	if len(args) < 2 {
		return fmt.Errorf("no arguments found, type lancs help for command list")
	}

	switch strings.ToLower(args[1]) {
	case "help", "-h", "--help":
		out.Out("HELP STRING HERE!\n\n")
		return nil

	case "start":
		err = a.start()

	case "stop":
		err = a.stop()

	case "ls", "list":
		err = a.list()

	case "init":
		err = a.init()

	case "add":
		err = a.add()

	case "remove":
		err = a.remove()
	default:
		out.Out("Wrong Argument, try \"PROGRAM_NAME help\" for help")
		err = fmt.Errorf("wrong arguments, program closing with no effect")
	}

	return err
}

func (a Application) start() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("start command starts daemon\n\n -p number or --port number  -  updates config to use number port")
			return nil
		}

		args := os.Args[2:]

		for len(args) != 0 {
			if args[0] == "-p" || args[0] == "--port" {
				var port int
				var err error
				if len(args) < 2 {
					return err
				}
				port, err = strconv.Atoi(args[1])
				if err != nil {
					return err
				}
				err = infrastructure.SetPort(port)
				if err != nil {
					return err
				}
				err = a.stopDaemon()
				if err != nil {
					return err
				}
				args = args[2:]
			}
		}
	}
	if _, _, err := processex.FindByName("lancsd"); err == processex.ErrNotFound {
		cmd := exec.Command("lancsd")
		if err := cmd.Start(); err != nil {
			log.Fatal(err)
		}
	} else {
		log.Printf("running instance found, if you want to update port, please stop and start lancsd")
	}
	return nil
}

func (a Application) stop() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("stop command stops daemon")
			return nil
		}
	}

	err := a.stopDaemon()

	return err
}

func (a Application) list() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("list command lists activeConfig rules\n\n rules  -  list rules\n hosts  -  list hosts\n port  -  shows active port")
			return nil
		}
		if os.Args[2] == "hosts" {
			hosts := infrastructure.GetHosts()

			if hosts == nil {
				return nil
			}

			a._out.Out(hosts)
			return nil
		}
		if os.Args[2] == "rules" {
			rules := infrastructure.GetRules()

			if rules == nil {
				return nil
			}

			a._out.Out(rules)
			return nil
		}
	}

	a._out.Out("list command lists activeConfig rules\n\n rules  -  list rules\n hosts  -  list hosts\n port  -  shows active port")
	return nil
}

func (a Application) init() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("init creates new config file, replacing old one\n\n -c tag address:port  -  sets host to connect to and used for downloading network parameters")
			return nil
		}
	}

	_loc := infrastructure.GetConfig()

	var err error

	err = os.MkdirAll(filepath.Dir(_loc), 0755)
	if err != nil {
		return err
	}
	_, err = os.Create(_loc)
	if err != nil {
		return err
	}

	if len(os.Args) > 2 {
		if os.Args[2] == "-c" {
			infrastructure.ClearHosts()
			_host := infrastructure.Host{
				Tag:  os.Args[3],
				IP:   strings.Split(os.Args[4], ":")[0],
				Port: strings.Split(os.Args[4], ":")[1]}
			infrastructure.SetHosts([]infrastructure.Host{_host})
		}
	} else {
		infrastructure.SetPort(32458)
	}
	return nil
}

func (a Application) add() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("add command allows to manually add rules and hosts.\nwill only have effect if daemon is running.\n\n rule dest\\src\\domain address\n\n host tag ip:port\n\n")
			return nil
		}

		port := strconv.Itoa(infrastructure.GetPort())
		if os.Args[2] == "host" {
			var _hostList infrastructure.HostList

			host := infrastructure.Host{
				Tag:  os.Args[3],
				IP:   strings.Split(os.Args[4], ":")[0],
				Port: strings.Split(os.Args[4], ":")[1]}

			resp, err := http.Get("http://127.0.0.1:" + port + "/hosts")
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				bodyBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					return err
				}

				err = json.Unmarshal(bodyBytes, &_hostList)
				if err != nil {
					return err
				}
			}

			_hostList.Hosts = append(_hostList.Hosts, host)
			_hostList.Version += 1

			hostData, err := json.Marshal(_hostList)
			if err != nil {
				return err
			}

			reader := strings.NewReader(string(hostData))
			req, err := http.NewRequest(http.MethodPut, "http://127.0.0.1:"+port+"/hosts", reader)
			if err != nil {
				return err
			}

			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(req)
			if err != nil {
				return err
			}
			defer response.Body.Close()
		}

		if os.Args[2] == "rule" {
			var rule infrastructure.Rule

			var _ruleList infrastructure.RuleList

			switch os.Args[3] {
			case "dest":
				rule.DestIP = os.Args[4]
			case "src":
				rule.SrcIP = os.Args[4]
			case "domain":
				rule.Domain = os.Args[4]
			default:
				return fmt.Errorf("wrong add rule format")
			}

			resp, err := http.Get("http://127.0.0.1:" + port + "/rules")
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				bodyBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					return err
				}

				err = json.Unmarshal(bodyBytes, &_ruleList)
				if err != nil {
					return err
				}
			}

			_ruleList.Rules = append(_ruleList.Rules, rule)
			_ruleList.Version += 1

			ruleData, err := json.Marshal(_ruleList)
			if err != nil {
				return err
			}

			reader := strings.NewReader(string(ruleData))
			req, err := http.NewRequest(http.MethodPut, "http://127.0.0.1:"+port+"/rules", reader)
			if err != nil {
				return err
			}

			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(req)
			if err != nil {
				return err
			}
			defer response.Body.Close()
		}
	}
	return nil
}

func (a Application) remove() error {
	if len(os.Args) > 2 {
		if os.Args[2] == "help" {
			a._out.Out("remove command allows to manually remove rules and hosts\n\n rule address\n\n host tag\n\n")
			return nil
		}

		port := strconv.Itoa(infrastructure.GetPort())

		if os.Args[2] == "host" {
			var _hostList infrastructure.HostList

			resp, err := http.Get("http://127.0.0.1:" + port + "/hosts")
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				bodyBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					return err
				}

				err = json.Unmarshal(bodyBytes, &_hostList)
				if err != nil {
					return err
				}
			}

			_index, _p := infrastructure.FindByTag(_hostList.Hosts, os.Args[3])
			if _p == nil {
				return fmt.Errorf("no host found by that tag")
			}
			_hostList.Hosts, err = infrastructure.RemoveByIndex[infrastructure.Host](_hostList.Hosts, _index)
			if err != nil {
				return err
			}
			_hostList.Version += 1

			hostData, err := json.Marshal(_hostList.Hosts)
			if err != nil {
				return err
			}

			reader := strings.NewReader(string(hostData))
			req, err := http.NewRequest(http.MethodPut, "http://127.0.0.1:"+port+"/hosts", reader)
			if err != nil {
				return err
			}

			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(req)
			if err != nil {
				return err
			}
			defer response.Body.Close()
		}

		if os.Args[2] == "rule" {
			var _ruleList infrastructure.RuleList

			resp, err := http.Get("http://127.0.0.1:" + port + "/rules")
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			if resp.StatusCode == http.StatusOK {
				bodyBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					return err
				}

				err = json.Unmarshal(bodyBytes, &_ruleList)
				if err != nil {
					return err
				}
			}

			_index, _p := infrastructure.FindByAddress(_ruleList.Rules, os.Args[3])
			if _p == *new(infrastructure.Rule) {
				return fmt.Errorf("no rule found by that tag")
			}
			_ruleList.Rules, err = infrastructure.RemoveByIndex[infrastructure.Rule](_ruleList.Rules, _index)
			if err != nil {
				return err
			}
			_ruleList.Version += 1

			ruleData, err := json.Marshal(_ruleList.Rules)
			if err != nil {
				return err
			}

			reader := strings.NewReader(string(ruleData))
			req, err := http.NewRequest(http.MethodPut, "http://127.0.0.1:"+port+"/rules", reader)
			if err != nil {
				return err
			}

			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(req)
			if err != nil {
				return err
			}
			defer response.Body.Close()
		}
	}
	return nil
}

func (a Application) stopDaemon() error {

	var err error
	var processes []*os.Process

	if processes, _, err = processex.FindByName("lancsd"); err == processex.ErrNotFound {
		log.Printf("no running instance found, nothing to stop")
	} else {
		for _, process := range processes {
			err = process.Signal(os.Interrupt)
		}
	}

	return err
}

package regulator_iptables

import (
	"fmt"
	"infrastructure"
	"net"
	"path/filepath"

	"github.com/singchia/go-xtables/iptables"
	"github.com/zhevron/go-config"
)

type IptablesRegulator struct {
	_activeRules []infrastructure.Rule
}

func (reg IptablesRegulator) Apply(rules []infrastructure.Rule) error {

	var err error
	err = nil

	ipt := iptables.NewIPTables().Table(iptables.TableTypeFilter).Chain(iptables.ChainTypeINPUT)

	for _, rule := range rules {
		if rule.DestIP != "" {
			if err = ipt.MatchSource(false, rule.DestIP).TargetDrop().Append(); err != nil {
				return err
			}
			reg._activeRules = append(reg._activeRules, rule)

		} else if rule.SrcIP != "" {
			if err = ipt.MatchDestination(false, rule.SrcIP).TargetDrop().Append(); err != nil {
				return err
			}
			reg._activeRules = append(reg._activeRules, rule)

		} else if rule.Domain != "" {
			if err = ipt.MatchDestination(false, reg.resolveDomain(rule.Domain)).TargetDrop().Append(); err != nil {
				return err
			}
			reg._activeRules = append(reg._activeRules, rule)

		} else {
			return fmt.Errorf("wrong rule format")
		}
	}

	return reg.updateConfig(err)
}

func (reg IptablesRegulator) resolveDomain(domain string) string {
	var destIP *net.IPAddr
	var err error
	if destIP, err = net.ResolveIPAddr("ip4", domain); err != nil {
		return ""
	}
	return destIP.IP.String()
}

func (reg IptablesRegulator) updateConfig(err error) error {
	if err != nil {
		return err
	}

	if err = config.LoadFileJSON(filepath.Join(infrastructure.GetConfig(), "lanscd.conf")); err != nil {
		return err
	}

	/*file, err := os.Open("/etc/lansc/lanscd.conf")
	if err != nil {
		return err
	}*/

	return nil
}

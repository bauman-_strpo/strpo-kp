module regulator_iptables

go 1.21.3

require github.com/singchia/go-xtables v1.0.0

require (
	github.com/singchia/go-hammer v0.0.2-0.20220516141917-9d83fc02d653 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
)

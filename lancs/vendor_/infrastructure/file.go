package infrastructure

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/zhevron/go-config"
)

func GetConfig() string {
	if os.Getenv("APPDATA") != "" {
		return filepath.Join(os.Getenv("APPDATA"), "lancs")
	}

	if os.Getenv("XDG_CONFIG_HOME") != "" {
		return filepath.Join(os.Getenv("XDG_CONFIG_HOME"), "lancs")
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(homeDir, ".config", "lancs")
}

func GetHosts() (hosts []Host) {

	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}
	hosts = config.Get("hosts", nil).([]Host)

	return hosts
}

func GetRules() (rules []Rule) {

	if err := config.LoadFileJSON(GetConfig()); err != nil {
		panic(err)
	}
	rules = config.Get("rules", nil).([]Rule)

	return rules
}

func AddRule(addedRules ...Rule) error {
	rules := GetRules()

	for _, rule := range addedRules {
		if _contains(rules, rule) {
			return fmt.Errorf("rule already exists")
		}
	}

	rules = append(rules, addedRules...)

	config.Set("rules", rules)

	err := config.SaveFileJSON(GetConfig())

	return err
}

func AddHost(host Host) error {
	hosts := GetHosts()

	if _, existingHost := FindByTag(hosts, host.Tag); existingHost != nil {
		existingHost.IPs = append(existingHost.IPs, host.IPs...)
		return nil
	}

	hosts = append(hosts, host)

	config.Set("hosts", hosts)

	err := config.SaveFileJSON(GetConfig())

	return err
}

func RemoveRule(rule Rule) error {
	rules := GetRules()

	if !_contains[Rule](rules, rule) {
		return fmt.Errorf("rule doesn't exist")
	}

	index, err := _find(rules, rule)
	if err != nil {
		return err
	}

	rules = remove[Rule](rules, index)

	config.Set("rules", rules)

	err = config.SaveFileJSON(GetConfig())

	return err
}

func RemoveHost(host Host) error {
	hosts := GetHosts()

	index, foundHost := FindByTag(hosts, host.Tag)

	if foundHost == nil {
		return fmt.Errorf("host doesn't exist")
	}

	hosts = remove[Host](hosts, index)

	config.Set("hosts", hosts)

	err := config.SaveFileJSON(GetConfig())

	return err
}

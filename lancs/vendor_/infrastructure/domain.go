package infrastructure

type Rule struct {
	DestIP string
	SrcIP  string
	Domain string
	Active bool
	Local  bool
}

type Host struct {
	Tag string
	IPs []string
}

func FindByTag(hosts []Host, tag string) (int, *Host) {
	for i, host := range hosts {
		if host.Tag == tag {
			return i, &host
		}
	}
	return 0, nil
}

func FindByIP(hosts []Host, ip string) (int, *Host) {
	for i, host := range hosts {
		for _, _ip := range host.IPs {
			if _ip == ip {
				return i, &host
			}
		}
	}
	return 0, nil
}

package infrastructure

import "fmt"

func _contains[T comparable](s []T, e T) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}

func remove[T interface{}](slice []T, s int) []T {
	return append(slice[:s], slice[s+1:]...)
}

func _find[T comparable](s []T, e T) (int, error) {

	for i, v := range s {
		if v == e {
			return i, nil
		}
	}
	return 0, fmt.Errorf("element not found")
}

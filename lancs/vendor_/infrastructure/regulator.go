package infrastructure

type Regulator interface {
	Apply(rules []Rule) error
}

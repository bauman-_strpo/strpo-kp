module infrastructure

go 1.21.3

require github.com/zhevron/go-config v0.0.0-20151201140412-697b1223812a

require (
	github.com/kr/pretty v0.3.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/zhevron/match v0.0.0-20151201130722-a611d3318b4c // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
